FROM python:3.8.5

WORKDIR /app

COPY . .

RUN pip install -r requirements.txt
RUN python -m grpc_tools.protoc -I ./proto --python_out=. --grpc_python_out=. ./proto/similarity.proto

EXPOSE 5000
CMD ["python", "server.py"]