# Similarity
## Description
'user_answer' 문장과 'answer' 문장들의 유사도를 비교하여 0.0 ~ 1.0 사이의 숫자로 수치화하고, 그 중 가장 높은 값을 return 하는 엔진(?)

> 'user_answer', 'answer'는 **similarity.proto** 참고   
## Setup
### Install dependency
```
pip install -r requirements.txt
```
### Build proto file
```
python -m grpc_tools.protoc -I ./proto --python_out=. --grpc_python_out=. ./proto/similarity.proto
```
### Build docker
```
sudo docker build -t docker.maum.ai:443/aics/similarity:<tag> .
sudo docker push docker.maum.ai:443/aics/similarity:<tag>
```

## RUN
### Python Server
```
python server.py
```
### Python Client
```
cd test
python client.py
```
### Docker
```
docker run \
--name collars-similarity \
-p 7886:5000 \
--restart unless-stopped \
-d docker.maum.ai:443/aics/similarity:<tag>
```