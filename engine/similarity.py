from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

class Similarity():
    def run(user_answer, answers):
        sentence = []
        sentence.append(user_answer)
        sentence += answers
        tfidf_vectorizer = TfidfVectorizer()
        tfidf_matrix = tfidf_vectorizer.fit_transform(sentence)
        text = tfidf_vectorizer.get_feature_names()
        idf = tfidf_vectorizer.idf_
        scores = []
        for i in range(len(sentence) - 1):
            s = cosine_similarity(tfidf_matrix[0:1], tfidf_matrix[i + 1:i + 2])
            scores.append(s[0][0])
        scores.sort()
        result = scores[-1]
        if result >= 1:
            result = 1
        return result