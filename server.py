from concurrent import futures

import grpc

import similarity_pb2_grpc
from similarity_pb2 import Output
from engine import similarity

SERVER_PORT = 5000


class SimilarityServicer(similarity_pb2_grpc.SimilarityServicer):
    def __init__(self):
        self.similarity = similarity.Similarity

    def GetScore(self, request, context):
        print("Start get score")
        user_answer = request.user_answer
        answers = request.answers
        result = self.similarity.run(user_answer, answers)
        return Output(data=result)


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=50))
    similarity_pb2_grpc.add_SimilarityServicer_to_server(SimilarityServicer(), server)
    print(f'Starting server. Listening on port {SERVER_PORT}.')
    server.add_insecure_port(f'[::]:{SERVER_PORT}')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    serve()
