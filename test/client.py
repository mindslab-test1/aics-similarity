import sys
sys.path.append('..')
import grpc
from similarity_pb2 import Input, Output
from similarity_pb2_grpc import SimilarityStub

# SERVER_URL = "127.0.0.1:7886"
# SERVER_URL = "114.108.173.102:7886"
# SERVER_URL = "114.108.173.114:7886"
# SERVER_URL = "aics-test.maum.ai/collar.Similarity/GetScore"
SERVER_URL = "aics-test.maum.ai:7886/similarity"
# SERVER_URL = "https://file.sangsangkids.co.kr:7886"

if __name__ == '__main__':
    with grpc.insecure_channel(f"{SERVER_URL}") as channel:
        stub = SimilarityStub(channel)

        input = Input()
        input.user_answer= 'Who am i?'
        input.answers.extend(['This is test', 'Who am121 i?', 'This is sparta', 'Ez Pz lemon squeezy', 'tS'])

        output = stub.GetScore(input)
        print(output)